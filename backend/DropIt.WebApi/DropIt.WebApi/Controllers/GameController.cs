﻿using DropIt.Core;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DropIt.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Game")]
    public class GameController : Controller
    {
        private IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }

        [HttpGet, Route("Winners")]        
        public ICollection<string> GetWinners()
        {
            return _gameService.GetWinners();
        }

        [HttpPost, Route("Winners")]
        public ICollection<string> SaveWinners()
        {
            return _gameService.GetWinners();
        }

        [HttpGet, Route("Update")]
        public ICollection<string> UpdateGame()
        {
            return _gameService.UpdateGame();
        }

    }
}