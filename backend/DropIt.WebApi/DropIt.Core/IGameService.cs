﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DropIt.Core
{
    public interface IGameService
    {
        ICollection<string> GetWinners();
        ICollection<string> UpdateGame();
        void SetWinner(string name, int level);
    }
}