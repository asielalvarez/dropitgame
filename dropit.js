var ball, basket;
var canvasWidth = 1100, canvasHeight = 650;

function startGame() {		
    myGameArea.start();
    ball = new ballComponent(35, 80, 30, "orange");
	basket = new basketComponent(0, 540, 100, 80, "GhostWhite");
}

var myGameArea = {
    canvas : document.createElement("canvas"),
    start : function() {
        this.canvas.width = canvasWidth;
        this.canvas.height = canvasHeight;
        this.context = this.canvas.getContext("2d");
        document.body.insertBefore(this.canvas, document.body.childNodes[0]);
        this.interval = setInterval(updateGameArea, 20);
    },
    clear : function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}

function ballComponent(x, y, radius, color) {
	this.x = x;
	this.y = y;
	
	this.update = function() {
		ctx = myGameArea.context;
		ctx.beginPath();
		ctx.arc(this.x, this.y, radius, 0, 2 * Math.PI);	
		ctx.fillStyle = color;
		ctx.fill();
		ctx.stroke();
	}
}

function basketComponent(x, y, width, height, color) {	
	this.x = x;
	this.y = y;
	
	this.update = function() { 
		ctx = myGameArea.context;				
		ctx.beginPath();		
		ctx.moveTo(this.x, this.y);
		ctx.lineTo(this.x + width - 10, this.y);
		ctx.lineTo(this.x + width - 10, this.y + height - 10);
		ctx.quadraticCurveTo(this.x + width - 10, this.y + height, this.x + width - 20, this.y + height);
		ctx.lineTo(this.x + 10, this.y + height);
		ctx.quadraticCurveTo(this.x, this.y + height, this.x, this.y + height - 10);
		ctx.lineTo(this.x, this.y);	
		ctx.fillStyle = color;
		ctx.fill();
		ctx.stroke();
		
		
	}
}

function updateGameArea() {
    myGameArea.clear();
    //basket.x += 0.1;
	//basket.y -= 0.5;
	//ball.y += 0.5;
    basket.update();
	ball.update();
}